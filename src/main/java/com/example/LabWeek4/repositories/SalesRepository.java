package com.example.LabWeek4.repositories;

import com.example.LabWeek4.models.SalesModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SalesRepository extends JpaRepository<SalesModel, UUID> {
}
