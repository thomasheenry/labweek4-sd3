package com.example.LabWeek4.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Column;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_PRODUCTS")
public class ProductModel extends Abstract {
    @Column(name = "NM_PRODUCTS", nullable = false)
    @Getter @Setter
    private String name;

    @Column(name = "PRICE_PRODUCTS", nullable = false)
    @Getter @Setter
    private Double price;

    @Column(name = "STOCK_PRODUCTS", nullable = false)
    @Getter @Setter
    private Integer stock;

}
