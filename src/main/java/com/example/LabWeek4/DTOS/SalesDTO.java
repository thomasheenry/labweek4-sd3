package com.example.LabWeek4.DTOS;

import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record SalesDTO (@NotNull UUID idProduct, @NotNull Integer amount) {}

