package com.example.LabWeek4.controllers;

import com.example.LabWeek4.DTOS.SalesDTO;
import com.example.LabWeek4.models.SalesModel;
import com.example.LabWeek4.services.SalesService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/vendas")
public class SalesController {
    @Autowired
    private SalesService salesService;

    @PostMapping
    public ResponseEntity<Object> saveSales(@RequestBody @Valid SalesDTO salesDTO){
        return salesService.saveSales(salesDTO);
    }

    @GetMapping
    public ResponseEntity<List<SalesModel>> getAllSales(){
        return ResponseEntity.status(HttpStatus.OK).body(salesService.getAllSales());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getSaleByID(@PathVariable(name = "id") UUID id){
        Optional<SalesModel> sale = salesService.getSaleByID(id);
        if (sale.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Sale not found");

        return ResponseEntity.status(HttpStatus.OK).body(sale.get());

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSale(@PathVariable(value = "id") UUID id){
        return salesService.deleteSales(id);
    }

}
