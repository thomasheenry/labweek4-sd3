package com.example.LabWeek4.controllers;

import com.example.LabWeek4.DTOS.ProductDTO;
import com.example.LabWeek4.models.ProductModel;
import com.example.LabWeek4.services.ProductsService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductsController {
    @Autowired
    private ProductsService productsService;

    @PostMapping
    public ResponseEntity<Object> saveproduct(@RequestBody @Valid ProductDTO product){
        return productsService.saveProduct(product);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateProduct(@RequestBody @Valid ProductDTO product, @PathVariable(value = "id") UUID id){
        return productsService.updateProduct(product,id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable(value = "id") UUID id){
        return productsService.deleteProduct(id);
    }

    @GetMapping
    public ResponseEntity<List<ProductModel>> getAllProducts(@PageableDefault(page = 0, size = 20, sort = "price",
            direction = Sort.Direction.ASC) Pageable pageable){
        return ResponseEntity.status(HttpStatus.OK).body(productsService.getAllProducts(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getProductByID(@PathVariable(name = "id") UUID id){
        Optional<ProductModel> product = productsService.getProductByID(id);

        if (ProductModel.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");

        return ResponseEntity.status(HttpStatus.OK).body(product.get());
    }

}
