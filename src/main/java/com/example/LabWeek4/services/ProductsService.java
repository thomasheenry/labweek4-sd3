package com.example.LabWeek4.services;

import com.example.LabWeek4.DTOS.ProductDTO;
import com.example.LabWeek4.models.ProductModel;
import com.example.LabWeek4.repositories.ProductRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;
public class ProductsService {
    @Autowired
    private ProductRepository productRepository;


    @Transactional
    public ResponseEntity<Object> saveProduct(ProductDTO productsDTO){
        ProductModel product = new ProductModel();
        BeanUtils.copyProperties(productsDTO, product);

        if (product.getPrice() <= 0) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid Price");

        productRepository.save(product);

        return ResponseEntity.status(HttpStatus.CREATED).body(product);
    }

    public List<ProductModel> getAllProducts(Pageable page){
        List<ProductModel> products = new ArrayList<>();

        productRepository.findAll(page).forEach(product ->{
            products.add(product);
        });

        return products;
    }

    public Optional<ProductModel> getProductByID(UUID id){
        return productRepository.findById(id);
    }

    public ResponseEntity<String> deleteProduct(UUID id){

        Optional<ProductModel> product = productRepository.findById(id);

        if (product.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");

        productRepository.delete(product.get());

        return ResponseEntity.status(HttpStatus.OK).body("Product deleted");
    }

    public ResponseEntity<Object> updateProduct(ProductDTO productDto, UUID id){
        Optional<ProductModel> product = productRepository.findById(id);

        if (product.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");

        ProductModel newProduct = product.get();
        BeanUtils.copyProperties(productDto, newProduct);
        newProduct.setUpdateAt(new Date());

        productRepository.save(newProduct);
        return ResponseEntity.status(HttpStatus.OK).body(newProduct);

    }

    public ResponseEntity<Object> updateStockProduct(UUID id, Integer amount){
        ProductModel product = productRepository.findById(id).get();
        if (product.getStock() <= 0 || product.getStock() < amount) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product don't have stock for this");

        product.setStock(product.getStock() - amount);
        product.setUpdateAt(new Date());

        return ResponseEntity.status(HttpStatus.OK).body("Stock updated");

    }

}
